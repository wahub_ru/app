"use strict";

/**
 *
 * Builder - сборщик тем дизайна для Webasyst
 *
 * @package  Builder
 * @author   Sergey Fedortsov <ajensen@wahub.ru>
 * @website  https://wahub.ru
 *
 **/

const path = require('path');
const themeID = path.basename(path.resolve());

module.exports = {
    // Используемые приложения
    apps: {
        site: true,
        blog: false,
        photos: false,
        shop: false,
        hub: false
    },

    // Конфигурация темы
    theme: {
        id: themeID,
        name: "Dummy",
        description: {
            site: "Тема дизайна «Dummy» для базового приложения «Cайт»",
            blog: "Тема дизайна «Dummy» для базового приложения «Блог»",
            photos: "Тема дизайна «Dummy» для базового приложения «Фото»",
            shop: "Тема дизайна «Dummy» для базового приложения «Магазин»",
            hub: "Тема дизайна «Dummy» для базового приложения «Хаб»"
        },
        version: {
            site: "1.0.0",
            blog: "1.0.0",
            photos: "1.0.0",
            shop: "1.0.0",
            hub: "1.0.0"
        },
        vendor: "1036600",
        author: "AJENSEN",
        instruction: "https://docs.wahub.ru/dummy",
        support: "support@wahub.ru"
    },

    // Препроцессор CSS
    styles: "styl",  // sass, scss, less, styl

    // Настройка сервера (https://www.browsersync.io/docs/options)
    server: {
        host: "localhost",
        proxy: "localhost",
        port: 3000,
        open: "external", // false, "local", "external", "ui", "tunnel"
        tunnel: false,
        notify: true
    }
};